package com.hcl.hackathon.model;

import java.time.LocalDateTime;

/*
 * Order Model class
 */
public class Order {

	private long userId;
	private String fromCcy; 
	private String toCcy;
	private double toSell;
	private LocalDateTime timwPlaced;
	private String originCountry;
	
	public Order() {
		super();
	}
	
	public Order(long userId, String fromCcy, String toCcy, double toSell, LocalDateTime timwPlaced,
			String originCountry) {
		super();
		this.userId = userId;
		this.fromCcy = fromCcy;
		this.toCcy = toCcy;
		this.toSell = toSell;
		this.timwPlaced = timwPlaced;
		this.originCountry = originCountry;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getFromCcy() {
		return fromCcy;
	}
	public void setFromCcy(String fromCcy) {
		this.fromCcy = fromCcy;
	}
	public String getToCcy() {
		return toCcy;
	}
	public void setToCcy(String toCcy) {
		this.toCcy = toCcy;
	}
	public double getToSell() {
		return toSell;
	}
	public void setToSell(double toSell) {
		this.toSell = toSell;
	}
	public LocalDateTime getTimwPlaced() {
		return timwPlaced;
	}
	public void setTimwPlaced(LocalDateTime timwPlaced) {
		this.timwPlaced = timwPlaced;
	}
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}	
}
