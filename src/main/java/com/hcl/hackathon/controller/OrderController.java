package com.hcl.hackathon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.hackathon.model.Order;
import com.hcl.hackathon.service.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	OrderService orderService;
	
	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<Void> consumeOrders(@RequestBody Order order){
		
		//orderService.consumeOrders(order);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
}
